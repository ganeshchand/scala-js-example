import org.scalajs.sbtplugin.ScalaJSPlugin.autoImport._
import sbt._

/**
  * Lists the dependencies for this project.
  */
object Dependencies {

  private val uPickleVersion = "0.4.4"
  private val autowireVersion = "0.2.6"
  private val sloggingVersion = "0.5.2"
  private val scalaTagsVersion = "0.6.5"

  object client {
    // JavaScript libraries the client uses
    val jsDependencies = Def.setting(Seq(
      // Used to add classes to HTML elements and also remove them: https://mvnrepository.com/artifact/org.webjars/jquery
      "org.webjars" % "jquery" % "3.1.1" / "3.1.1/jquery.min.js"
    ))

    // The triple % gets the library in two versions: One for running on the JVM and one for running on a JavaScript engine like V8
    val scalaJsDependencies = Def.setting(Seq(
      // Used to produce HTML and CSS with Scala on the client side: https://github.com/lihaoyi/scalatags
      "com.lihaoyi" %%% "scalatags" % scalaTagsVersion,

      // Serializes data between client and server: https://github.com/lihaoyi/upickle-pprint
      "com.lihaoyi" %%% "upickle" % uPickleVersion,

      // Type-safe Ajax calls between client and server: https://github.com/lihaoyi/autowire
      "com.lihaoyi" %%% "autowire" % autowireVersion,

      // A type facade for jQuery so we can use the JavaScript library in a type-safe manner: https://github.com/scala-js/scala-js-jquery
      "be.doeraene" %%% "scalajs-jquery" % "0.9.1",

      // Logging: https://github.com/jokade/slogging
      "biz.enef" %%% "slogging" % sloggingVersion
    ))
  }

  val server = {
    // Akka HTTP has a dependency on akka-actor. This akka-actor version must match the one of akka-slf4j
    val akkaActorVersion = "2.4.19"
    // For the newest version see: http://doc.akka.io/docs/akka-http/current/scala/http/index.html
    val akkaHttpVersion = "10.0.10"
    Def.setting(Seq(
      // Our HTTP server: http://doc.akka.io/docs/akka-http/current/index.html
      // It's important that the Akka actor library used by Akka HTTP and by other dependencies such as Akka SLF4J is
      // the same, otherwise the application will throw a NoSuchMethodError at startup.
      "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,

      // The server creates HTML pages and CSS that it sends to the client
      "com.lihaoyi" %% "scalatags" % scalaTagsVersion,

      // Serializes data between client and server: https://github.com/lihaoyi/upickle-pprint
      "com.lihaoyi" %% "upickle" % uPickleVersion,
      // Type-safe Ajax calls between client and server: https://github.com/lihaoyi/autowire
      "com.lihaoyi" %% "autowire" % autowireVersion,

      // Logging --------------
      // Logging facade for Scala and Scala.js: https://github.com/jokade/slogging
      "biz.enef" %% "slogging-slf4j" % sloggingVersion,
      // Logging backend: http://logback.qos.ch/
      "ch.qos.logback" % "logback-classic" % "1.1.8",
      // Akka, which provides our HTTP server, logs using SLF4J: https://mvnrepository.com/artifact/com.typesafe.akka/akka-slf4j_2.12
      "com.typesafe.akka" %% "akka-slf4j" % akkaActorVersion,
      // Needed for reading the logback.groovy configuration file
      "org.codehaus.groovy" % "groovy-all" % "2.4.7",

      // Database connection ---------------------

      // Hikari Connection Pool has a dependency on Slick
      // https://mvnrepository.com/artifact/com.typesafe.slick/slick-hikaricp_2.12
      "com.typesafe.slick" %% "slick-hikaricp" % "3.2.1",
      // https://mvnrepository.com/artifact/org.postgresql/postgresql
      "org.postgresql" % "postgresql" % "42.1.4",

      // Testing -------------------
      "org.scalatest" %% "scalatest" % "3.0.1" % "test",
      // Used to tests our routes, for example: https://mvnrepository.com/artifact/com.typesafe.akka/akka-http-testkit_2.12
      "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVersion % "test",
      // For executing HTTP requests that test our application: https://github.com/scalaj/scalaj-http
      "org.scalaj" %% "scalaj-http" % "2.3.0" % "test",
      // Helps us verify our HTTP responses: https://github.com/ruippeixotog/scala-scraper
      "net.ruippeixotog" %% "scala-scraper" % "2.0.0" % "test"
    ))
  }
}
